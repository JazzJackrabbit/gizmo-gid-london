import RPi.GPIO as GPIO
from time import sleep
#import machine
import distanceSensor as sensor
import gizmoUtilities as utility

TURN_TIME = 2
SENSOR_DELAY = 1

frontSensor = sensor.KRDistanceSensor("Front sensor")

GPIO.setmode(GPIO.BCM)

try:
	while 1:
		utility.printTimestamp()
		frontSensor.updateDistance()
		print "Distance",frontSensor.distance,"cm"
		sleep(SENSOR_DELAY)
except KeyboardInterrupt:
	print "Program terminated from keyboard."
	utility.programShutdown()
except:
	print "Oops. Something went wrong."
	utility.programShutdown()
