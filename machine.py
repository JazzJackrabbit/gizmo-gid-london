#!/usr/bin/python

import motorBase as motor
import distanceSensor as sensor
import random
import gizmoUtilities as utility

from time import sleep

class KRMachine(object):
	def __init__(self, motorBaseDelay, a1, b1, e1, a2, b2, e2, a3, b3, e3, a4, b4, e4):
		self.motorBase = motor.KRMotorBase(motorBaseDelay, a1, b1, e1, a2, b2, e2, a3, b3, e3, a4, b4, e4)
		self.frontSensor = sensor.KRDistanceSensor("Front sensor",38,40)
	def move(self):
		self.updateSensorData()
		frontDistance = self.getFrontSensorData()
		if (frontDistance < 25):
			self.stop()
			# utility.coinFlip(self.turnLeft, self.turnRight)
			# self.frontSensor.distance = random.randrange(0, 300)
                else:
                    self.goForward()
	def turnLeft(self):
		self.motorBase.turnLeft()
	def turnRight(self):
		self.motorBase.turnRight()
	def turnAround(self):
		self.motorBase.turnAround()
	def stop(self):
		self.motorBase.stop()
	def goForward(self):
		self.motorBase.goForward()
	def updateSensorData(self):
		self.frontSensor.updateDistance()
	def getFrontSensorData(self):
		return self.frontSensor.distance


