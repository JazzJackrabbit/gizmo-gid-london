import RPi.GPIO as GPIO
import time

class KRSensor(object):
	"""docstring for KRSensor"""
	def __init__(self, trig = 20, echo=21):
		GPIO.setmode(GPIO.BCM)
		self.TRIG = trig
		self.ECHO = echo
		print "Distance Measurement is Progress"
		GPIO.setup(TRIG,GPIO.OUT)
		GPIO.setup(ECHO,GPIO.IN)
		GPIO.output(TRIG,False)
		time.sleep(1)

	def detect(self):
		GPIO.output(TRIG,True)
		time.sleep(0.00001)
		GPIO.output(TRIG,False)

		while (GPIO.input(ECHO)==0):
			pulse_start = time.time()

		while (GPIO.input(ECHO)==1):
			pulse_end = time.time()

		pulse_duration = pulse_end - pulse_start

		distance = pulse_duration * 17150

		distance = round(distance,2)

		print "Distance",distance,"cm"

		return distance
