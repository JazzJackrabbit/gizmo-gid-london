import math
import pyaudio

class KRAudio(object):
	def __init__(self):
		self._pyAudio = pyaudio.PyAudio

	def beep(self, frequency, length):
		#See http://en.wikipedia.org/wiki/Bit_rate#Audio
		BITRATE = 16000 #number of frames per second/frameset.

		#See http://www.phy.mtu.edu/~suits/notefreqs.html
		self._frequency = frequency #Hz, waves per second, 261.63=C4-note.
		self._length = length #seconds to play sound

		NUMBEROFFRAMES = int(BITRATE * self._length)
		RESTFRAMES = NUMBEROFFRAMES % BITRATE
		WAVEDATA = ''

		for x in xrange(NUMBEROFFRAMES):
 			WAVEDATA = WAVEDATA+chr(int(math.sin(x/((BITRATE/self._frequency)/math.pi))*127+128))

		#fill remainder of frameset with silence
		for x in xrange(RESTFRAMES):
 			WAVEDATA = WAVEDATA+chr(128)

		p = self._pyAudio()
		stream = p.open(format = p.get_format_from_width(1),
                channels = 1,
                rate = BITRATE,
                output = True)
		stream.write(WAVEDATA)
		stream.stop_stream()
		stream.close()
		p.terminate()
