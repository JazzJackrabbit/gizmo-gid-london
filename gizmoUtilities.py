#!/usr/bin/python
import datetime
import random
import RPi.GPIO as GPIO

def printTimestamp():
	print "\n"+"-- "+datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M:%S")+" --"

def programShutdown():
	print "Shutdown triggered."
	GPIO.cleanup()

def coinFlip(heads,tails):
	r = random.randrange(0,2)
	if (r == 0):
		heads()
	elif (r == 1):
		tails()
