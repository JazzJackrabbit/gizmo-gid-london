#!/usr/bin/python

#import gizmoGPIO
import motor
from time import sleep

class KRMotorBase(object):
	def __init__(self, delay, a1, b1, e1, a2, b2, e2, a3, b3, e3, a4, b4, e4):
		# super(KRMotorBase, self).__init__()
		self.motorTopLeft = motor.KRMotor("TopLeft", a1, b1, e1)
		self.motorTopRight = motor.KRMotor("TopRight", a2, b2, e2)
		self.motorBottomLeft = motor.KRMotor("BottomLeft", a3, b3, e3)
		self.motorBottomRight = motor.KRMotor("BottomRight", a4, b4, e4)
		self.delay = delay

	def goForward(self):
		print "[^] Machine moving FORWARD"
		self.motorTopLeft.turnOn()
		self.motorTopRight.turnOn()
		self.motorBottomLeft.turnOn()
		self.motorBottomRight.turnOn()

	def turnLeft(self):
		print "[<] Machine turning LEFT"
		self.motorTopLeft.turnOff()
		self.motorBottomLeft.turnOff()
		self.motorTopRight.turnOn()
		self.motorBottomLeft.turnOn()
		print "Waiting "+str(self.delay)+" seconds"
		sleep(self.delay)
		self.goForward()

	def turnRight(self):
		print "[>] Machine turning RIGHT"
		self.motorTopLeft.turnOn()
		self.motorBottomLeft.turnOn()
		self.motorTopRight.turnOff()
		self.motorBottomLeft.turnOff()
		print "Waiting "+str(self.delay)+" seconds"
		sleep(self.delay)
		self.goForward()

	def stop(self):
		print "Machine stopping"
		self.motorTopLeft.turnOff()
		self.motorTopRight.turnOff()
                self.motorBottomLeft.turnOff()
                self.motorBottomRight.turnOff()
