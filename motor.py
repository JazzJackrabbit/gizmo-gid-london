#!/usr/bin/python

import RPi.GPIO as GPIO
from time import sleep

class KRMotor:
	def __init__(self, name, a, b, e):
		self.name = name
		self.isOn = False
                self.a = a
                self.b = b
                self.e = e
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(self.a,GPIO.OUT)
                GPIO.setup(self.b,GPIO.OUT)
                GPIO.setup(self.e,GPIO.OUT)

	def isOn():
		return self.isOn

	def turnOn(self):
                GPIO.setmode(GPIO.BOARD)
                GPIO.output(self.a, GPIO.HIGH)
                GPIO.output(self.b, GPIO.LOW)
                GPIO.output(self.e, GPIO.HIGH)
		self.isOn = True
		print "Motor '"+self.name+"' is now turned ON."

	def turnOff(self):
		self.isOn = False
                GPIO.setmode(GPIO.BOARD)
                GPIO.output(self.a, GPIO.LOW)
                GPIO.output(self.b, GPIO.LOW)
                GPIO.output(self.e, GPIO.LOW)
		print "Motor '"+self.name+"' is now turned OFF."

	def togglePower(self):
		if (self.isOn == True):
			self.turnOff()
		elif (self.isOn == False):
			self.turnOn()
