#!/usr/bin/python

import RPi.GPIO as GPIO
from time import sleep
import machine
import distanceSensor as sensor
import gizmoUtilities as utility
import pygame

TURN_TIME = 5
SENSOR_DELAY = 0.5

Mo1A = 16
Mo1B = 18
Mo1E = 22

Mo2A = 23
Mo2B = 21
Mo2E = 19

Mo3A = 33
Mo3B = 35
Mo3E = 37

Mo4A = 15
Mo4B = 13
Mo4E = 11

machine = machine.KRMachine(TURN_TIME, Mo1A, Mo1B, Mo1E, Mo2A, Mo2B, Mo2E, Mo3A, Mo3B, Mo3E, Mo4A, Mo4B, Mo4E)

##MANUAL MODE
# try:
# 	while 1:
# 		data = int(raw_input('Input (0 - stop, 1 - right, 2 - left, 3 - forward): '))
# 		if (data == 0):
# 			machine.stop()
# 		elif (data == 1):
# 			machine.turnRight()
# 		elif (data == 2):
# 			machine.turnLeft()
# 		elif (data == 3):
# 			machine.goForward()
# 		elif (data < 0):
# 			machine.stop()
# 			utility.programShutdown()
# 			break
# except KeyboardInterrupt:
# 	print "Program terminated from keyboard."
# 	utility.programShutdown()
# except:
# 	print "Oops. Something went wrong."
# 	utility.programShutdown()

#AUTOMATIC MODE
try:
	pygame.init()
	pygame.mixer.music.load("music.wav")
	pygame.mixer.music.play()
	while 1:
		utility.printTimestamp()
		machine.move()
		sleep(SENSOR_DELAY)
except KeyboardInterrupt:
	print "Program terminated from keyboard."
	utility.programShutdown()
except:
	print "Oops. Something went wrong."
	utility.programShutdown()
