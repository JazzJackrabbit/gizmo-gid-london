#!/usr/bin/python

#import gizmoGPIO
import RPi.GPIO as GPIO
import random
import time

GPIO.setmode(GPIO.BOARD)

class KRDistanceSensor(object):
	def __init__(self, name, trig, echo):
                self._distance = 999
		self._name = name
		self.trig = trig
		self.echo = echo
		GPIO.setup(self.trig,GPIO.OUT)
		GPIO.setup(self.echo,GPIO.IN)
		GPIO.output(self.trig,False)
		time.sleep(1)

	@property
	def distance(self):
		return self._distance
	@distance.setter
	def distance(self,value):
		self._distance = value

	def updateDistance(self):
		print "Updating sensor '"+self._name+"' data"
		self._distance = self.detect()
		print self._name+" data: "+str(self._distance)

	def detect(self):
		GPIO.output(self.trig,True)
		time.sleep(0.00001)
		GPIO.output(self.trig,False)

		while (GPIO.input(self.echo)==0):
			pulse_start = time.time()

		while (GPIO.input(self.echo)==1):
			pulse_end = time.time()

		pulse_duration = pulse_end - pulse_start

		distance = pulse_duration * 17150

		distance = round(distance,2)

		#print "Distance",distance,"cm"

		return distance
