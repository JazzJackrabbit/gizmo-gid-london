import RPi.GPIO as GPIO
from time import sleep
import sound3 as s3
import distanceSensor as sensor
import gizmoUtilities as utility

GPIO.setmode(GPIO.BOARD)

TURN_TIME = 5
SENSOR_DELAY = 0.25

print "Here we go"

beeper = s3.KRAudio()
frontSensor = sensor.KRDistanceSensor("Front sensor",38,40)

sleep(2)

try:
	while 1:
		frontSensor.updateDistance()
		frontDistance = frontSensor.distance
		utility.printTimestamp()
		beeper.beep(frontDistance*50,SENSOR_DELAY*0.75)
		sleep(SENSOR_DELAY)
except KeyboardInterrupt:
	print "Program terminated from keyboard."
	utility.programShutdown()
except:
	print "Oops. Something went wrong."
	utility.programShutdown()
